CREATE TABLE tickets
( ticket_no      char( 13 )    NOT NULL, 
  book_ref       char( 6 ) 	   NOT NULL, 
  passenger_id   varchar( 20 ) NOT NULL, 
  passenger_name text          NOT NULL, 
  contact_data   jsonb		   NULL, 
  PRIMARY KEY ( ticket_no ),
  FOREIGN KEY ( book_ref )
	REFERENCES bookings ( book_ref )
);