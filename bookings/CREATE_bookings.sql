CREATE TABLE bookings
( book_ref     char( 6 ) 	  	NOT NULL,
  book_date    timestamptz 		NOT NULL,
  total_amount numeric( 10, 2 ) NOT NULL,
  PRIMARY KEY ( book_ref )
);