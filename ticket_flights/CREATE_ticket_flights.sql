CREATE TABLE ticket_flights
( ticket_no 	  char( 13 )       NOT NULL, 
  flight_id 	  integer 		   NOT NULL, 
  fare_conditions varchar( 10 )    NOT NULL, 
  amount 		  numeric( 10, 2 ) NOT NULL, 
  CHECK ( amount >= 0 ),
  CHECK ( fare_conditions IN ( 'Economy', 'Comfort', 'Business' ) ),
  PRIMARY KEY ( ticket_no, flight_id ),
  FOREIGN KEY ( flight_id )
    REFERENCES flights ( flight_id ),
  FOREIGN KEY ( ticket_no )
	REFERENCES tickets ( ticket_no )
);